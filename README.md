![Framework Logo](/assets/Framework.png)
<p align="center">
A Java library collection of multiple niche library expansions including Minecraft and Discord
</p>

[![](https://jitpack.io/v/Negative-Games/Framework.svg)](https://jitpack.io/#Negative-Games/Framework) ![licence](https://img.shields.io/github/license/negative-games/framework) ![GitHub commits since latest release (by date)](https://img.shields.io/github/commits-since/negative-games/framework/latest) ![GitHub Workflow Status](https://img.shields.io/github/workflow/status/negative-games/framework/CodeQL)
## Contributing

Contributions are always welcome!

See `contributing.md` for ways to get started.

Please adhere to this project's `code of conduct`.


## Documentation
Documentation Portal: https://docs.frameworklib.com/
Minecraft Frameowork JavaDocs: https://framework.docs.negative.games 


## License

[We use the MIT licence for this project. Please read it here.](https://choosealicense.com/licenses/mit/)
## Support

For support, join https://discord.negative.games, create an issue card or email us at negativegames.dev@gmail.com.


## Feedback

If you have any feedback, please reach out to us at https://discord.negative.games
